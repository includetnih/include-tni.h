import sqlite3 as lite
import sys
from datetime import datetime

def insertNewData(ADC_Val, Pic_URL):
    con = lite.connect('TESA_DATABASE.db')
    with con:
        print ('database connected')
        now = datetime.now()
        
        cur = con.cursor()
        print ('starting upload @ %s:%s %s-%s-%s' % (now.hour,now.minute,now.day,now.month,now.year))

        command = 'insert into raw_data values(NULL, %d, \'%s\', \'%s:%s_%s-%s-%s\');' %(ADC_Val,Pic_URL,now.hour,now.minute,now.day,now.month,now.year)
        cur.execute(command)
        print (command)

        print ('uploaded')

    if con:

        con.close()

def getLastUploadId():
    con = lite.connect('TESA_DATABASE.db')
    with con:
        print ('database connected')
        
        cur = con.cursor()

        cur.execute('select max(id) from raw_data')
        lastid = cur.fetchone()[0]

        print ('fetch complete')
        return lastid

    if con:

        con.close()

def printAllRawData():
    con = lite.connect('TESA_DATABASE.db')
    with con:
        print ('database connected')
        
        cur = con.cursor()

        cur.execute('select * from raw_data')
        rows = cur.fetchall()

        for row in rows:
            print (row)

    if con:

        con.close()

def eraseDatabase():
    con = lite.connect('TESA_DATABASE.db')
    with con:
        print ('database connected')
        
        cur = con.cursor()

        cur.execute('delete from raw_data')
        cur.execute('delete from sqlite_sequence where name=\'rawdata\'')

        print ('erase database completed')
        
    if con:

        con.close()
