#import camera
import database
import Modbus
import time
import picamera
import toheroku

camera = picamera.PiCamera()
#camera.resolution(640, 480)

while 1 :
    try:
        ADCData = Modbus.getSensorData()

        Id = database.getLastUploadId()
        if Id :
            Id = Id+1
        else:
            Id = 1
        print (Id)


        locate = 'tree_%s.jpg' %(str(Id))
        camera.capture(locate)
        print ('capture!')

        database.insertNewData(ADCData,locate)
        database.printAllRawData()

        toheroku.uploaddata(ADCData,locate)
        
    except Exception as e :
        print (str(e))

    time.sleep(1)
