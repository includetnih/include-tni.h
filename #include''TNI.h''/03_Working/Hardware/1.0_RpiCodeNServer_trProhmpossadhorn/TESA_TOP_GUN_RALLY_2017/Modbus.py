import serial
import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_rtu

def getSensorData():
        print ('getting sensor data via Modbus')
        PORT = '/dev/ttyACM0'
        master = modbus_rtu.RtuMaster(
            serial.Serial(port=PORT, baudrate=115200, bytesize=8, parity='N', stopbits=1, xonxoff=0)
        )
        master.set_timeout(5.0)
        master.set_verbose(True)
        data = master.execute(15, cst.READ_INPUT_REGISTERS, 0, 1)
        print ('Modbus data : %s' % (str(data[0])))
        return data[0]
    try:
        pass:

    except Exception:
        pass:
