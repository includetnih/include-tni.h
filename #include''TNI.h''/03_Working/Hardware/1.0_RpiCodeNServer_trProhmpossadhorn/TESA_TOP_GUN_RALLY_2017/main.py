import camera
import database
import Modbus

ADCData = Modbus.getSensorData()

#ADCData = 1000;

Id = database.getLastUploadId()
if Id :
    Id = Id+1
else:
    Id = 1
print (Id)

camera.capture(str(Id))
picname ='tree_%s.jpg' %(str(Id))

database.insertNewData(ADCData,picname)
database.printAllRawData()
