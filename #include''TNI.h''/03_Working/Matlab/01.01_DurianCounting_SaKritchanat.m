%% clear data
tic
clear all;
clc;
%close all;

%% intitial
raw = imread('durian3.jpg');
si = size(raw);
% dup = raw;
% %dup(dup(:,:,3)<(dup(:,:,1)+dup(:,:,2))/4) = 255;
data{1} = {raw,'raw'};
% figure();
% imshow(dup);

%% analyze

gray = rgb2gray(raw);
data{2} = {gray,'gray'};

R = raw(:,:,1);
R1 = zeros(si(1),si(2));
R1(R<95) = 0;
R1(R>=95) = 1;

G = raw(:,:,2);
B = raw(:,:,3);
RG = double(R)./double(G);
RG1 = zeros(si(1),si(2));
RG1(RG>1 & RG<1.22 &B<160) = 1;

Sti = zeros(si(1),si(2));
Sti(RG>1.2 & RG<1.6 &B>80) = 1;

fill1 = R1+RG1-Sti;

inv = zeros(si(1),si(2));
inv(fill1 ==0) = 1;
se90 = strel('line', 2, 90);
se0 = strel('line', 1, 0);
sei45 = strel('line', 1, -45);
se45 = strel('line', 1, 45);
lineB = imdilate(inv, [se0 se90]);
lineB = imdilate(lineB, [sei45 se45]);

sha = zeros(si(1),si(2));
% sha(RG>1 & RG<1.22 &B<60 & (R+G+B)/3 <70 & ((R(R,R+6)+G(G,G+6)+B(B,B+6)+R(R,R-6)+G(G,G-6)+B(B,B-6)+R(R+6,R)+G(G+6,G)+B(B+6,B)+R(R-6,R)+G(G-6,G)+B(B-6,B))/12 - (R+G+B)/3 )< 10)  = 1;
% sha(RG>1 & RG<1.22 &B<60 & (R+G+B)/3 <70 & ((R(sha,sha+6)+G(sha,sha+6)+B(sha,sha+6)+R(sha,sha-6)+G(sha,sha-6)+B(sha,sha-6)+R(sha+6,sha)+G(sha+6,sha)+B(sha+6,sha)+R(sha-6,sha)+G(sha-6,sha)+B(sha-6,sha))/12 - (R+G+B)/3 )< 10)  = 1;
sha(RG>1 & RG<1.22 &B<60 & (R+G+B)/3 <80)=1;
sha(R > 160 & R < 170 & G>145 & G < 160 & B < 89 ) = 1;
shaR = lineB + sha;

% imtool(shaR);

pre =  edge(G,'prewitt',0.2);
 mix = pre+shaR;
 
 mix1 = imfill(mix,'holes');
 
inv2 = zeros(si(1),si(2));
inv2(mix1 ==0) = 1;

fin = imerode(inv2,strel('diamond',1));
fin = imerode(fin,strel('rectangle',[3,1]));

for i = 1:2
fin2 =  edge(fin,'prewitt',0.2);
temp  = imfill(fin2,'holes');
fin = fin + fin2;
end
for i = i:2
fin = imerode(fin,strel('sphere',2));
end
fin  = imfill(fin,'holes');

inv3 = zeros(si(1),si(2));
inv3(fin ==0) = 1;
fin = imerode(inv3,strel('sphere',2));

%% start

imdurian = imread('durian3.jpg');
%imtool(imdurian)

%% Go to gray

imred   = imdurian (:,:,1);
imgreen = imdurian (:,:,2);
imblue  = imdurian (:,:,3);
imgray  = rgb2gray (imdurian);
% figure(),imshow(SUM1); title('SUM');
%% Binary & Analyre

levelr = 0.2;
levelg = 0.1;
levelb = 0.4;

a = im2bw(imred,levelr);
b = im2bw(imgreen,levelg);
c = im2bw(imblue,levelb);
% d = im2bw(imgray);
sum1 = (a&b+c);



%% Try edge
edge5 = edge(sum1,'Sobel');
%figure(),imshow(edge5); title('Sobel');

se90 = strel('line',3,90);
se0  = strel('line',3,0);
imdil = imdilate(edge5, [se90 se0]);
%figure(),imshow(imdil);

imf = imfill(imdil,'holes');
%figure(),imshow(imf);

imco = imcomplement(imf);
sum2 = sum1+imco;
%figure(),imshow(sum2);

edge10 = edge(sum2,'Sobel');

se90 = strel('line',5,90);
se0  = strel('line',5,0);
imdil2 = imdilate(edge10, [se90 se0]);
%figure(),imshow(imdil2);

imf2 = imfill(imdil,'holes');
%figure(),imshow(imf2);

imco2 = imcomplement(imf2);
g = im2bw(imgreen);

sum3 = g&imco2;
%figure(),imshow(sum3);

imf3 = imfill(sum3,'holes');
%figure(),imshow(imf3);

sum4 = sum3+imf3+c&g&sum1&sum2;


%% new
total = zeros(si(1),si(2));
invf = zeros(si(1),si(2));
invf(fin ==0) = 1;
invs = zeros(si(1),si(2));
invs(shaR ==0) = 1;

total((double(sum1)+double(sum4)+2*double(invf)+2*double(invs))/6 >0.5) = 1;
final = imopen(total,strel('diamond',1));
final = imopen(final,strel('sphere',1));
final = imopen(final,strel('sphere',2));

temp1 = zeros(si(1),si(2));
tem1 = final(1:187,1:250);
tem1 = imclose(tem1,strel('sphere',2));
tem2 = final(188:375,1:250);
tem2 = imclose(tem2,strel('sphere',2));
tem3 = final(1:187,251:500);
tem3 = imopen(tem3,strel('sphere',2));
tem3 = imclose(tem3,strel('sphere',2));
tem4 = final(188:375,251:500);
tem4 = imclose(tem4,strel('diamond',2));
 se90 = strel('line',1,90);
se0  = strel('line',2,0);
tem4 = imdilate(tem4, [se90 se0]);

final(1:187,1:250) = tem1;
final(188:375,1:250)= tem2;
final(1:187,251:500) = tem3;
final(188:375,251:500) = tem4;
labeledImage = bwlabel(final, 8);
coloredLabels = label2rgb (labeledImage, 'hsv', 'k', 'shuffle'); 

boldM = regionprops(labeledImage, gray, 'all');
numberOfBlobs = size(boldM, 1);

binaryImage = imbinarize(gray);
boun = bwboundaries(binaryImage);
numberOfBoundaries = size(boun, 1);
for k = 1 : numberOfBoundaries
	thisBoundary = boun{k};
	plot(thisBoundary(:,2), thisBoundary(:,1), 'g', 'LineWidth', 2);
end

blobECD = zeros(1, numberOfBlobs);
n=1;
for k = 1 : numberOfBlobs           
	blobArea = boldM(k).Area;
	blobCentroid = boldM(k).Centroid;	
    if(blobArea>500)
        Area(n) = blobArea;
        Cen(n,:) = blobCentroid;
        n=n+1;
    end
end

cenX = Cen(:,1);
cenY = Cen(:,2);
sii = size(Area); 

 %% output
 imshow(raw);
for k = 1 : sii(2)   
	text(cenX(k)-7, cenY(k), num2str(k),'FontSize', 30, 'FontWeight', 'Bold');
end
 %% to database
 conn = database('','SaKritchanat','tharm1');

