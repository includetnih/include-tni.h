function varargout = WeatherForecasting0001(varargin)
% WEATHERFORECASTING0001 MATLAB code for WeatherForecasting0001.fig
%      WEATHERFORECASTING0001, by itself, creates a new WEATHERFORECASTING0001 or raises the existing
%      singleton*.
%
%      H = WEATHERFORECASTING0001 returns the handle to a new WEATHERFORECASTING0001 or the handle to
%      the existing singleton*.
%
%      WEATHERFORECASTING0001('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WEATHERFORECASTING0001.M with the given input arguments.
%
%      WEATHERFORECASTING0001('Property','Value',...) creates a new WEATHERFORECASTING0001 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before WeatherForecasting0001_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to WeatherForecasting0001_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help WeatherForecasting0001

% Last Modified by GUIDE v2.5 14-Mar-2017 17:54:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @WeatherForecasting0001_OpeningFcn, ...
                   'gui_OutputFcn',  @WeatherForecasting0001_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before WeatherForecasting0001 is made visible.
%%openfunction
function WeatherForecasting0001_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to WeatherForecasting0001 (see VARARGIN)

% Choose default command line output for WeatherForecasting0001
    xlabel('Date');
    ylabel('Chance of rain')
    set(handles.OutText, 'string','Watering?') 

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes WeatherForecasting0001 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = WeatherForecasting0001_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure




function FreqVal_Callback(hObject, eventdata, handles)
% hObject    handle to FreqVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% Hints: get(hObject,'String') returns contents of FreqVal as text
%        str2double(get(hObject,'String')) returns contents of FreqVal as a double


% --- Executes during object creation, after setting all properties.
function FreqVal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FreqVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%call back nonthaburi
% --- Executes on button press in nonthaburi.
function nonthaburi_Callback(hObject, eventdata, handles)
% hObject    handle to nonthaburi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
url = 'http://api.wunderground.com/api/7a4649fdcf9bd1eb/forecast10day/q/Nonthaburi.json';
data = webread(url);
plotpop(data,handles);





function OutText_Callback(hObject, eventdata, handles)
% hObject    handle to OutText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% Hints: get(hObject,'String') returns contents of OutText as text
%        str2double(get(hObject,'String')) returns contents of OutText as a double


% --- Executes during object creation, after setting all properties.
function OutText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OutText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%call back chantaburi
% --- Executes on button press in chan.
function chan_Callback(hObject, eventdata, handles)
% hObject    handle to chan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
url = 'http://api.wunderground.com/api/7a4649fdcf9bd1eb/forecast10day/q/Chantaburi.json';
data = webread(url);
plotpop(data,handles);



%%call back rayong
% --- Executes on button press in rayong.
function rayong_Callback(hObject, eventdata, handles)
% hObject    handle to rayong (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
url = 'http://api.wunderground.com/api/7a4649fdcf9bd1eb/forecast10day/q/Rayong.json';
data = webread(url);
plotpop(data,handles);

%%diciding and plot function
function plotpop(data,handles)
pop = [data.forecast.simpleforecast.forecastday.pop].';
for i = 1:5
    date(i) = [data.forecast.simpleforecast.forecastday(i).date.day];  
end
date = date';
plot(date,pop(1:5))
axis([date(1) date(5) 0 100])
if pop(2)>40
    set(handles.OutText, 'string','watering') 
else 
    set(handles.OutText, 'string','Do Not Watering') 
end


% --- Executes during object creation, after setting all properties.
function text3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
