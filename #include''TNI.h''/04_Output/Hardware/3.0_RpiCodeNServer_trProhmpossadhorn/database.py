import sqlite3 as lite

def insertNewData(ADC_Val, Pic_URL,tim):
    con = lite.connect('TESA_DATABASE.db')
    with con:
        print ('database connected')
        
        cur = con.cursor()
        print ('starting upload @ %s' % (tim))

        command = 'insert into raw_data values(NULL, %d, \'%s\', \'%s\');' %(ADC_Val,Pic_URL,tim)
        cur.execute(command)
        print (command)

        print ('uploaded')

    if con:

        con.close()

def getLastUploadId():
    con = lite.connect('TESA_DATABASE.db')
    with con:
        print ('database connected')
        
        cur = con.cursor()

        cur.execute('select max(id) from raw_data')
        lastid = cur.fetchone()[0]

        print ('fetch complete')
        return lastid

    if con:

        con.close()

def printAllRawData():
    con = lite.connect('TESA_DATABASE.db')
    with con:
        print ('database connected')
        
        cur = con.cursor()

        cur.execute('select * from raw_data')
        rows = cur.fetchall()

        for row in rows:
            print (row)

    if con:

        con.close()

def eraseDatabase():
    con = lite.connect('TESA_DATABASE.db')
    with con:
        print ('database connected')
        
        cur = con.cursor()

        cur.execute('delete from raw_data')
        cur.execute('delete from sqlite_sequence where name=\'raw_data\'')

        print ('erase database completed')
        
    if con:

        con.close()
