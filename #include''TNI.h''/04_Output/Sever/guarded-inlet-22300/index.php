<?php

$proxy = 'velodrome.usefixie.com:80';
$proxyauth = 'fixie:DVHb9WmDvqT8YCv';
$access_token = '5s3beWIw+vG+G1W0C4svPIUEN//gmACmJKSTVhI9N2ieM9bqMNYdVKY2bLWumtoofFFcHdU7c9L4bVlXZmZuqlF8H9RQQLwIHn3J+rrdwIqjY8b/LyBg98tLI5PpcICcmwYknf+LaxUVGTOTPfyphgdB04t89/1O/w1cDnyilFU=';

// Get POST body content
$content = file_get_contents('php://input');

// Parse JSON
$events = json_decode($content, true);

// Validate parsed JSON data
if (!is_null($events['events']))
{
	// Loop through each event
	foreach ($events['events'] as $event)
	{
		// Reply only when message sent is in 'text' format
		if ($event['type'] == 'message' && $event['message']['type'] == 'text')
		{
			// Get replyToken
			$replyToken = $event['replyToken'];
			$user_id = $event['source']['userId'];
			// Build message to reply back
			
			if($event['message']['text'] == "UserId")
			$messages = [
				'type' => 'text',
				'text' => $user_id
			];

			elseif($event['message']['text'] == "info")
			{
				$chx = curl_init();
				curl_setopt($chx, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($chx, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($chx, CURLOPT_URL, 'https://include-tni-h.herokuapp.com/');
				$resultx = curl_exec($chx);
				curl_close($chx);

				$obj = json_decode($resultx, true);
				$result_text = 'ความชื้นของดิน : '.end($obj[1])[1]."%\n".'สภาพอากาศ : '.end($obj[0])[2]."\n".'ความกดอากาศ : '.end($obj[0])[3].' hpa'."\n".'ความชื้นในอากาศ : '.end($obj[0])[4]."\n".'อุณหภูมิ : '
				.end($obj[0])[1].' °C';

				$messages = ['type'=>'text','text'=>$result_text];
			}

			elseif($event['message']['text'] == "รูป")
			$messages = [
				'type' => 'image',
 				'originalContentUrl' => 'https://guarded-inlet-22300.herokuapp.com/1.jpg',
   				'previewImageUrl' => 'https://guarded-inlet-22300.herokuapp.com/2.jpg'
			];
			else
			$messages = [
				'type' => 'text',
				'text' => 'Invalid Input'
			];

			// Make a POST Request to Messaging API to reply to sender
			$url = 'https://api.line.me/v2/bot/message/reply';
			$data = [
				'replyToken' => $replyToken,
				'messages' => [$messages]
			];
			$post = json_encode($data);
			$headers = array('Content-Type: application/json', 'Authorization: Bearer ' . $access_token);
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_PROXY, $proxy);
			curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
			$result = curl_exec($ch);
			curl_close($ch);

			if($event['message']['text'] == "info")
			{
				$messages = [
					'type' => 'image',
					'originalContentUrl' => 'https://guarded-inlet-22300.herokuapp.com/1.jpg',
					'previewImageUrl' => 'https://guarded-inlet-22300.herokuapp.com/2.jpg'
				];
				$url = 'https://api.line.me/v2/bot/message/push';
				$data = [
					'to' => $user_id,
					'messages' => [$messages]
				];
				$post = json_encode($data);
				$headers = array('Content-Type: application/json', 'Authorization: Bearer ' . $access_token);
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_PROXY, $proxy);
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
				$result = curl_exec($ch);
				curl_close ($ch);
			}

		}
	}
}
