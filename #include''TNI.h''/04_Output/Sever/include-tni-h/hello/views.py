from django.shortcuts import render
from django.http import HttpResponse
from django.db import connection

from .models import Greeting
import requests
import psycopg2
import json
import csv
# Create your views here.

def index(request):
    #weather
    #conn = psycopg2.connect("dbname='d9tqtbptnknicc' user='ymtgbzdghoanjt' host='ec2-23-23-222-147.compute-1.amazonaws.com' password='6d95d7830f79731c06c9e1632369d3b33d011cffccd71bc484fc0777bb0ecd3f'")

    #TGR2017
    conn = psycopg2.connect("dbname='dchus0belk8eie' user='wlyuglhpngztyv' host='ec2-23-21-227-73.compute-1.amazonaws.com' password='6cc1c67c615aca2ddcd205a9a7d964ca454d43d22796da1497ccd27cb0221f4e'")

    cur = conn.cursor()

    data = requests.get('http://api.wunderground.com/api/8831039b6b1094a3/forecast/conditions/q/zmw:00000.1.48456.json').json()

    date = str(data['forecast']['txt_forecast']['date'])
    temp = str(data['current_observation']['temp_c'])
    forecast = str(data['forecast']['txt_forecast']['forecastday'][0]['fcttext_metric'])
    pressure = str(data['current_observation']['pressure_mb'])
    relative_humidity = str(data['current_observation']['relative_humidity'])
    a = "'"
    #cur.execute("DELETE FROM forecast_data")

    forecast = forecast.split('.',1)

    cur.execute("INSERT INTO forecast_data VALUES (" + a + date + a + ", " + a + temp + a + ", " + a + forecast[0] + a + ", " + a + pressure + a + ", " + a + relative_humidity + a + ")")
    #text = date + "<br>Temperature : " + temp + " Celsius<br>Weather : " + forecast + "<br>Pressure : " + pressure + " hpa"

    tt=[]
    tt.append([])
    tt.append([])

    sql = """SELECT * FROM forecast_data"""
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
        tt[0].append(row)

    sql = """SELECT * FROM raw_data"""
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
        tt[1].append(row)

    j = json.dumps(tt)

    """fp = open("includetnih_data.csv","w")
    c = csv.writer(fp)
    c.writerow(row)
    fp.close()"""

    conn.commit()
    conn.close()
    return HttpResponse(j)
    #return render(request, 'index.html')

def db(request):

    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, 'db.html', {'greetings': greetings})
